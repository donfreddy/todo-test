import express from "express";
import todoRepo from "../database/repositories/todoRepo.js";

const router = express.Router();

// get all todo items in the db
router.get("/", async (req, res) => {
  let todos = await todoRepo.findAll();

  res.json(todos);
});

// add a todo item
router.post("/create", async (req, res) => {
  const { title, description, done, dueDate } = req.body;

  const todo = {
    title: title,
    description: description,
    done: done,
    dueDate: dueDate,
  };

  createTodo = await todoRepo.create(todo);

  res.json(createTodo);
});

// update a todo item
router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const { title, description, done, dueDate } = req.body;

  const todo = await todoRepo.findById(id);

  if (!todo) throw new Error("Todo does exist");

  if (title) todo.title = title;
  if (description) todo.description = description;
  if (done) todo.done = done;
  if (dueDate) todo.dueDate = dueDate;

  todoRepo.updateById(id, todo);

  res.status(200).json([]);
});

// delete a todo item
router.delete("/:id", async (req, res) => {
  const { id } = req.params;

  console.log(`Deleted record with id: ${id}`);

  await todoRepo.deleteById(id);

  res.status(200).json([]);
});

export default router;
