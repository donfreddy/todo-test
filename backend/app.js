import express from "express";
import createError from "http-errors";
import logger from "morgan";
import cors from "cors";
import "./database/index.js"; // initialize database
import todoRoutes from "./routes/todoRoute.js";

const app = express();

app.use(logger("dev"));

app.use(express.json({ limit: "10mb" }));
app.use(
  express.urlencoded({ limit: "10mb", extended: true, parameterLimit: 50000 })
);
app.use(cors());

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Todos API." });
});

// Routes
app.use("/todos", todoRoutes);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// Middleware Error Handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

export default app;
