import mongoose from "mongoose";

// MongoDB configuration
const db = {
  name: process.env.DB_NAME || "todos-db",
  host: process.env.DB_HOST || "localhost",
  port: process.env.DB_PORT || "27017",
};

// Build the connection string
const dbURI = `mongodb://${db.host}:${db.port}/${db.name}`;

const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  autoIndex: true, //todo: Set to false in production.
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
  socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
};

// Create the database connection
mongoose
  .connect(dbURI, options)
  .then(() => {
    console.info("Mongoose connection done");
  })
  .catch((e) => {
    console.info("Mongoose connection error");
    console.error(e);
  });

// CONNECTION EVENTS

// When successfully connected
mongoose.connection.on("connected", () => {
  console.info("Mongoose default connection open to " + dbURI);
});

// If the connection throws an error
mongoose.connection.on("error", (err) => {
  console.error("Mongoose default connection error: " + err.message);
});

// When the connection is disconnected
mongoose.connection.on("disconnected", () => {
  console.info("Mongoose default connection disconnected");
});
